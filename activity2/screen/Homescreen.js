import { View, Text } from "react-native";
import React from "react";

const HomeScreen = () => {
    
    return (
    <View

    style={{
        height: "90%",
        backgroundColor: "whitesmoke",
        justifyContent: "center",
        alignItems: "center",

    }}
    > <Text style={{ fontSize: 35, fontWeight: "bold", letterSpacing: 5 ,color: '#3E2723'}}>
        Manacpo, Janice C. </Text>
    </View>
    );
};

export default HomeScreen;