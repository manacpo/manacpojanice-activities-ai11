import { View, Text } from "react-native";
import React from "react";

const ProfileScreen = () => {
    return (
    <View
    style={{
        height: "90%",
        backgroundColor: "yellow",
        justifyContent: "center",
        alignItems: "center",
    
    }}
    >
    <Text style={{ fontSize: 35, fontWeight: "bold", letterSpacing: 4 ,color: "#3E2723"}}> 

        Completed Tasks Go Here </Text>
    </View>
    
    );
};

export default ProfileScreen;
