import React, { Component } from 'react'
import { View, SafeAreaView, Text, StyleSheet } from 'react-native'

const Style = ({ state }) => (
  <View style={styles.container}>
    <SafeAreaView style={styles.safe}>
      <Text
        style={styles.display}
        adjustsFontSizeToFit
        numberOfLines={1}
      >{state.display}</Text>
      { state.result !== '' &&
        <Text
          style={styles.result}
          adjustsFontSizeToFit
          numberOfLines={1}
        >{state.result}</Text>
      }
    </SafeAreaView>
  </View>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5dc',
    flex: 1,
    justifyContent: 'space-around',
    paddingHorizontal: 24,
  },
  safe: {
    flex: 1,
    justifyContent: 'space-around',
  },
  display: {
    textAlign: 'right',
    fontWeight: 'normal',
    color: '#7fff00',
    fontSize: 40,
  },
  result: {
    textAlign: 'right',
    fontWeight: 'bold',
    color: '#0000ff',
    fontSize: 30,
    
  },
})

export default Style;