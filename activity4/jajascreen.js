import React from "react";
import { Image, Text, View } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

// Janice Manacpo
const slides = [
    {
    key: "first",
    title: "WHY I CHOSE IT COURSE?", 
    text: 
    "Actually I dont have any plan and choices to what course I want in college, But stop studying is not on my option so I just go with the flow and do my job to enroll, exam and interview in college with defferent course in every school and let  God do the rest. ",
    image: require("./Image/home.png"),
    },
    {
    key: "second",
    title: "WHY I CHOSE IT COURSE",
    text:
    "I meditate and find my will that makes me choose IT. For  I am hook and amaze to those programers who fight cybercrime, and It gives me rush of undreline watching them when I once go  in  Region 8 cybercrime main office.",
    image: require("./Image/explore.png"), 
    },
    {
    key: "third",
    title: "WHY I CHOSE IT COURSE",
    text:
    "Then I also Think practicaly, and search a high salary and on demand job not only now but also in the future.",
    image: require("./Image/investment.png"),
    },
    {
    key: "fourth",
    title: "WHY I CHOSE IT COURSE",
    text:
    "Another reason is I choose the course and school that I can grow mentally and physically.",
    image: require("./Image/dream.png"),
    },
    {
    key: "fifth",
    title: "WHY I CHOSE IT COURSE",
    text:
    "Over all the reason why I choose this course is I want to serve people using the knowledge I know and skills I have and I could have .",
    image: require("./Image/Researching.png"),
    },
    {
    key: "last",
    title: "WHY I CHOSE IT COURSE",
    text:
    "for I know that the most dangerous place and weapon now in present time is internet and tehnology, and hope someday I could be one of those cybercrime fighter and security.",
    image: require("./Image/end.png"),
    },
]

const Slide = ({ item }) => {
    return (
    <View style={{ flex: 1, backgroundColor: 'white'}}>
        <Image source={item.image}
        style={{
            resizeMode: 'center',
            height: "70%",
            width: "100%",
        }}
        />
        <Text style={{
            paddingTop: 5,
            paddingBottom: 5,
            fontSize: 20,
            fontWeight: "bold",
            color: "#cd00cd",
            alignSelf: "center",
        }}
        >
        {item.title}
        </Text>

        <Text style={{
        textAlign:"center",
        color:"#000000",
        fontSize:15,
        paddingHorizontal:20,
        }}>
        {item.text}
        </Text>
    </View>
    )
    };

    export default function IntroSliders({navigation}) {
    return(
        <AppIntroSlider
        renderItem={({item}) => <Slide item={item}/> } 
        data={slides} 
        activeDotStyle={{
            backgroundColor:"#FF7272",
            width:15
        }}
        onDone={() => navigation.push('Intro')}
        showDoneButton={true}
        renderDoneButton={()=> <Text style={{color: '#696969', fontWeight: 'bold', margin: 1, fontSize: 15}}>
            Home</Text>}
        showNextButton={true}
        renderNextButton={()=> <Text style={{color: '#00005f', fontWeight: 'bold', margin: 1, fontSize: 15}}>
            Continue</Text>}
        /> 
    ) 
};